COVERPROFILE       := test/output/coverprofile.out
PROVIDER_NAME      := warren.io
REPO_ROOT          := $(shell dirname $(realpath $(lastword ${MAKEFILE_LIST})))
VERSION            := $(shell cat "${REPO_ROOT}/VERSION")
LD_FLAGS_CMD       := "hack/get-build-ld-flags.sh k8s.io/component-base $(REPO_ROOT)/VERSION"
CONTROL_NAMESPACE  := mcm-provider-dev-test
CONTROL_NAMESPACE  := shoot--local--p7fui1z6q4
#CONTROL_NAMESPACE  := shoot--foobar--warren
CONTROL_KUBECONFIG := dev/control-kubeconfig.yaml
TARGET_KUBECONFIG  := dev/target-kubeconfig.yaml

#########################################
# Rules for starting machine-controller locally
#########################################

.PHONY: start
start:
	@GO111MODULE=on go run \
			-mod=vendor \
		    -ldflags "-w $(shell $(shell echo ${LD_FLAGS_CMD}))" \
			cmd/machine-controller-manager-provider-warren/main.go \
			--control-kubeconfig=$(CONTROL_KUBECONFIG) \
			--target-kubeconfig=$(TARGET_KUBECONFIG) \
			--namespace=$(CONTROL_NAMESPACE) \
			--machine-creation-timeout=20m \
			--machine-drain-timeout=5m \
			--machine-health-timeout=10m \
			--machine-pv-detach-timeout=2m \
			--machine-safety-apiserver-statuscheck-timeout=30s \
			--machine-safety-apiserver-statuscheck-period=1m \
			--machine-safety-orphan-vms-period=30m

.PHONY: debug
debug:
			dlv debug cmd/machine-controller-manager-provider-warren/main.go --\
			--control-kubeconfig=$(CONTROL_KUBECONFIG) \
			--target-kubeconfig=$(TARGET_KUBECONFIG) \
			--namespace=$(CONTROL_NAMESPACE) \
			--machine-creation-timeout=20m \
			--machine-drain-timeout=5m \
			--machine-health-timeout=10m \
			--machine-pv-detach-timeout=2m \
			--machine-safety-apiserver-statuscheck-timeout=30s \
			--machine-safety-apiserver-statuscheck-period=1m \
			--machine-safety-orphan-vms-period=30m

#########################################
# Rules for re-vendoring
#########################################

.PHONY: revendor
revendor:
	@GO111MODULE=on go mod tidy -compat=1.18
	@GO111MODULE=on go mod vendor

#########################################
# Rules for testing
#########################################

.PHONY: test
test:
	@hack/test.sh

.PHONY: test-cov
test-cov:
	@hack/test.sh --coverage

.PHONY: test-clean
test-clean:
	@hack/test.sh --clean --coverage

#########################################
# Rules for build/release
#########################################

.PHONY: build-local
build-local:
	@env LD_FLAGS="-w $(shell $(shell echo ${LD_FLAGS_CMD}))" LOCAL_BUILD=1 hack/build.sh

.PHONY: build
build:
	@env LD_FLAGS="-w $(shell $(shell echo ${LD_FLAGS_CMD}))" hack/build.sh

.PHONY: clean
clean:
	@rm -rf bin/
