/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package warren contains the Warren Platform specific implementation to manage machines
package warren

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"math/rand"
	"net"
	"strings"
	"time"

	"github.com/gardener/machine-controller-manager/pkg/util/provider/driver"
	"github.com/gardener/machine-controller-manager/pkg/util/provider/machinecodes/codes"
	"github.com/gardener/machine-controller-manager/pkg/util/provider/machinecodes/status"
	"gitlab.com/warrenio/library/go-client/warren"
	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren/apis"
	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren/apis/transcoder"
	"k8s.io/klog/v2"
)

// CreateMachine handles a machine creation request
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The create request for VM creation
func (p *MachineProvider) CreateMachine(ctx context.Context, req *driver.CreateMachineRequest) (*driver.CreateMachineResponse, error) {
	const op = "warren/CreateMachine"

	klog.V(4).Infof("%s called: %+v", op, req)
	defer klog.V(5).Infof("%s call completed", op)

	extendedCtx := context.WithValue(ctx, CtxWrapDataKey("MethodData"), &CreateMachineMethodData{})

	resp, err := p.createMachine(extendedCtx, req)
	if nil != err {
		if _, ok := err.(*status.Status); !ok {
			err = status.Error(codes.Internal, err.Error())
		}

		p.createMachineOnErrorCleanup(extendedCtx, req, err)
	}

	return resp, err
}

// createMachine handles the actual machine creation request without cleanup
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The create request for VM creation
func (p *MachineProvider) createMachine(ctx context.Context, req *driver.CreateMachineRequest) (*driver.CreateMachineResponse, error) {
	var (
		machine      = req.Machine
		machineClass = req.MachineClass
		secret       = req.Secret
		resultData   = ctx.Value(CtxWrapDataKey("MethodData")).(*CreateMachineMethodData)
	)

	if machine.Spec.ProviderID != "" {
		return nil, status.Error(codes.InvalidArgument, "Machine creation with existing provider ID is not supported")
	}

	providerSpec, err := transcoder.DecodeProviderSpecFromMachineClass(machineClass, secret)
	if nil != err {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	userData, ok := secret.Data["userData"]
	if !ok {
		return nil, errors.New("userData doesn't exist")
	}

	client := apis.GetClientForTokenAndEndpoint(string(secret.Data["warrenToken"]), providerSpec.APIEndpointURL)
	zone := client.LocationSlug

	if zone != "" && zone != providerSpec.Zone {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Zone requested does not match expected client location: %q != %q", zone, providerSpec.Zone))
	}

	server, _ := apis.GetServerWithName(ctx, client, machine.Name)
	if nil != server {
		var errorCode codes.Code
		isCleanupAvailable := p.createMachineValidateDeploymentAndCleanup(ctx, req, server)

		if isCleanupAvailable {
			errorCode = codes.Aborted
		} else {
			errorCode = codes.AlreadyExists
		}

		return nil, status.Error(errorCode, "Server already exists")
	}

	var floatingIP *warren.FloatingIp

	if providerSpec.FloatingPoolName != "" {
		name := fmt.Sprintf("%s-%s-ipv4", machine.Name, providerSpec.FloatingPoolName)

		floatingIP, err = apis.GetFloatingIPByName(ctx, client, name)
		if errors.Is(err, apis.ErrFloatingIPNotFound) {
			floatingIP, err = client.Network.CreateFloatingIp(&warren.CreateFloatingIpRequest{Name: warren.New(name)})
			if nil != err {
				return nil, status.Error(codes.Internal, apis.GetFloatingIPErrorFromHttpCallError(err).Error())
			}

			resultData.FloatingIPID = floatingIP.Id
		} else if nil != err {
			return nil, status.Error(codes.Internal, err.Error())
		}
	}

	image, err := apis.GetImageWithName(ctx, client, providerSpec.ImageName)
	if nil != err {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	password := providerSpec.Password

	if password == "" {
		var passwordBuilder strings.Builder
		seededRand := rand.New(rand.NewSource(time.Now().Unix()))

		for i := 0; i < (warrenPasswordGeneratedLength - 3); i++ {
			passwordBuilder.WriteRune(rune(32 + seededRand.Intn(94)))
		}

		// Lower-case letter
		passwordBuilder.WriteRune(rune(97 + seededRand.Intn(26)))
		// Upper-case letter
		passwordBuilder.WriteRune(rune(65 + seededRand.Intn(26)))
		// number
		passwordBuilder.WriteRune(rune(48 + seededRand.Intn(10)))

		password = passwordBuilder.String()
	}

	username := providerSpec.Username

	if username == "" {
		username = warrenDefaultVMUsername
	}

	volumeSize := int(math.Ceil(float64(providerSpec.VolumeSize) / 1073741824))

	runCmd := strings.Split(string(userData), "\n\n")

	if nil != providerSpec.ExtraConfig {
		for k, v := range providerSpec.ExtraConfig {
			runCmd = append(runCmd, fmt.Sprintf("sysctl %q=%q", k, v))
		}
	}

	userDataJsonEnc, err := json.Marshal(map[string]interface{}{"runcmd": runCmd})
	if nil != err {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	createReq := &warren.CreateVirtualMachineRequest{
		Name:            warren.New(machine.Name),
		OsName:          warren.New(image.OsName),
		OsVersion:       warren.New(image.OsVersion),
		Disks:           warren.New(volumeSize),
		VCpu:            warren.New(int(providerSpec.Cores)),
		Ram:             warren.New(int(providerSpec.Memory)),
		ReservePublicIp: warren.New(false),
		Backup:          warren.New(false),
		Username:        warren.New(username),
		Password:        warren.New(password),
		PublicKey:       warren.New(providerSpec.SSHKey),
		CloudInit:       warren.New(string(userDataJsonEnc)),
	}

	if providerSpec.NetworkUUID != "" {
		createReq.NetworkUuid = warren.New(providerSpec.NetworkUUID)
	}

	server, err = client.VirtualMachine.CreateVirtualMachine(createReq)
	if nil != err {
		return nil, apis.GetServerErrorFromHttpCallError(err)
	}

	if nil != floatingIP {
		_, err = client.Network.AssignFloatingIp(net.ParseIP(floatingIP.Address), server.Uuid)
		if nil != err {
			return nil, status.Error(codes.Internal, apis.GetFloatingIPErrorFromHttpCallError(err).Error())
		}
	}

	response := &driver.CreateMachineResponse{
		ProviderID: transcoder.EncodeProviderID(providerSpec.Zone, server.Uuid),
		NodeName:   server.Name,
	}

	return response, nil
}

// createMachineOnErrorCleanup cleans up a failed machine creation request
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The create request for VM creation
// err error                        Error encountered
func (p *MachineProvider) createMachineValidateDeploymentAndCleanup(ctx context.Context, req *driver.CreateMachineRequest, server *warren.VirtualMachine) bool {
	var (
		machine      = req.Machine
		machineClass = req.MachineClass
		secret       = req.Secret
	)

	providerSpec, err := transcoder.DecodeProviderSpecFromMachineClass(machineClass, secret)
	if nil != err {
		return false
	}

	serverZone, err := transcoder.DecodeZoneFromProviderID(machine.Spec.ProviderID)
	if nil != err {
		return false
	}

	isCleanupAvailable := true

	if providerSpec.Zone != serverZone {
		isCleanupAvailable = false
	}

	if isCleanupAvailable {
		client := apis.GetClientForTokenAndEndpoint(string(secret.Data["warrenToken"]), providerSpec.APIEndpointURL)
		resultData := ctx.Value(CtxWrapDataKey("MethodData")).(*CreateMachineMethodData)

		resultData.ServerUUID = server.Uuid

		if providerSpec.FloatingPoolName != "" {
			name := fmt.Sprintf("%s-%s-ipv4", machine.Name, providerSpec.FloatingPoolName)

			floatingIP, _ := apis.GetFloatingIPByName(ctx, client, name)
			if floatingIP != nil {
				resultData.FloatingIPID = floatingIP.Id
			}
		}
	}

	return isCleanupAvailable
}

// createMachineOnErrorCleanup cleans up a failed machine creation request
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The create request for VM creation
// err error                        Error encountered
func (p *MachineProvider) createMachineOnErrorCleanup(ctx context.Context, req *driver.CreateMachineRequest, err error) {
	var (
		client *warren.Client

		secret = req.Secret
	)

	providerSpec, err := transcoder.DecodeProviderSpecFromMachineClass(req.MachineClass, secret)
	if nil == err {
		client = apis.GetClientForTokenAndEndpoint(string(secret.Data["warrenToken"]), providerSpec.APIEndpointURL)
	} else {
		client = apis.GetClientForToken(string(secret.Data["warrenToken"]))
	}

	resultData := ctx.Value(CtxWrapDataKey("MethodData")).(*CreateMachineMethodData)

	server := &warren.VirtualMachine{}
	if resultData.ServerUUID != "" {
		server, _ = client.VirtualMachine.GetByUuid(resultData.ServerUUID)
	} else {
		server, _ = apis.GetServerWithName(ctx, client, req.Machine.Name)
	}

	if nil != server {
		_ = client.VirtualMachine.DeleteVm(server.Uuid)
	}

	if resultData.FloatingIPID != 0 {
		floatingIP, _ := apis.GetFloatingIPByID(ctx, client, resultData.FloatingIPID)
		if nil != floatingIP {
			_ = client.Network.DeleteFloatingIp(net.ParseIP(floatingIP.Address))
		}
	}
}

// DeleteMachine handles a machine deletion request
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The delete request for VM deletion
func (p *MachineProvider) DeleteMachine(ctx context.Context, req *driver.DeleteMachineRequest) (*driver.DeleteMachineResponse, error) {
	var (
		err    error
		server *warren.VirtualMachine

		machine      = req.Machine
		machineClass = req.MachineClass
		secret       = req.Secret
	)

	const op = "warren/DeleteMachine"

	klog.V(4).Infof("%s called: %+v", op, req)
	defer klog.V(5).Infof("%s call completed", op)

	providerSpec, err := transcoder.DecodeProviderSpecFromMachineClass(machineClass, secret)
	if nil != err {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	client := apis.GetClientForTokenAndEndpoint(string(secret.Data["warrenToken"]), providerSpec.APIEndpointURL)

	if machine.Spec.ProviderID != "" {
		var serverUUID string

		serverUUID, err = transcoder.DecodeServerUUIDFromProviderID(machine.Spec.ProviderID)
		if nil != err {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		server, err = client.VirtualMachine.GetByUuid(serverUUID)

		if nil != err {
			err = apis.GetServerErrorFromHttpCallError(err)
		}
	} else if machine.Name != "" {
		server, err = apis.GetServerWithName(ctx, client, machine.Name)
	} else {
		err = errors.New("Provider ID and machine name given are valid")
	}

	if nil != err {
		if errors.Is(err, apis.ErrServerNotFound) {
			klog.V(3).Infof("VM %s does not exist", machine.Name)
			return &driver.DeleteMachineResponse{}, nil
		}

		return nil, status.Error(codes.InvalidArgument, apis.GetServerErrorFromHttpCallError(err).Error())
	}

	err = client.VirtualMachine.DeleteVm(server.Uuid)
	if nil != err {
		return nil, status.Error(codes.Unavailable, apis.GetServerErrorFromHttpCallError(err).Error())
	}

	if providerSpec.FloatingPoolName != "" {
		name := fmt.Sprintf("%s-%s-ipv4", machine.Name, providerSpec.FloatingPoolName)

		floatingIP, err := apis.GetFloatingIPByName(ctx, client, name)
		if nil != err && !errors.Is(err, apis.ErrFloatingIPNotFound) {
			return nil, status.Error(codes.Internal, err.Error())
		} else if nil != floatingIP {
			client.Network.DeleteFloatingIp(net.ParseIP(floatingIP.Address))
			if nil != err {
				return nil, status.Error(codes.Unavailable, apis.GetFloatingIPErrorFromHttpCallError(err).Error())
			}
		}
	}

	return &driver.DeleteMachineResponse{}, nil
}

// GetMachineStatus handles a machine get status request
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The get request for VM info
func (p *MachineProvider) GetMachineStatus(ctx context.Context, req *driver.GetMachineStatusRequest) (*driver.GetMachineStatusResponse, error) {
	var (
		err        error
		server     *warren.VirtualMachine
		serverUUID string

		machine      = req.Machine
		secret       = req.Secret
		machineClass = req.MachineClass
	)

	const op = "warren/GetMachineStatus"

	klog.V(4).Infof("%s called: %+v", op, req)
	defer klog.V(5).Infof("%s call completed", op)

	providerSpec, err := transcoder.DecodeProviderSpecFromMachineClass(machineClass, secret)
	if nil != err {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	client := apis.GetClientForTokenAndEndpoint(string(secret.Data["warrenToken"]), providerSpec.APIEndpointURL)

	if machine.Spec.ProviderID != "" {
		serverUUID, err = transcoder.DecodeServerUUIDFromProviderID(machine.Spec.ProviderID)
		if nil != err {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		server, err = client.VirtualMachine.GetByUuid(serverUUID)
		if nil != err {
			err = apis.GetServerErrorFromHttpCallError(err)
		}
	} else {
		// Handle case where machine lookup occurs with empty provider ID
		server, err = apis.GetServerWithName(ctx, client, machine.Name)

		if server == nil {
			return nil, status.Error(codes.NotFound, fmt.Sprintf("Provider ID for machine %q is not defined (%q)", machine.Name, err))
		}

		serverUUID = server.Uuid
	}
	if nil != err {
		errorCode := codes.Unavailable

		if errors.Is(err, apis.ErrServerNotFound) {
			errorCode = codes.NotFound
		}

		return nil, status.Error(errorCode, err.Error())
	} else if server == nil {
		return nil, status.Error(codes.NotFound, fmt.Sprintf("VM %s (%s) does not exist", machine.Name, serverUUID))
	}

	providerID := transcoder.EncodeProviderID(providerSpec.Zone, serverUUID)
	return &driver.GetMachineStatusResponse{ProviderID: providerID, NodeName: server.Name}, nil
}

// ListMachines lists all the machines possibilly created by a providerSpec
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The request object to get a list of VMs belonging to a machineClass
func (p *MachineProvider) ListMachines(ctx context.Context, req *driver.ListMachinesRequest) (*driver.ListMachinesResponse, error) {
	const op = "warren/ListMachines"

	klog.V(4).Infof("%s called: %+v", op, req)
	defer klog.V(5).Infof("%s call completed", op)

	providerSpec, err := transcoder.DecodeProviderSpecFromMachineClass(req.MachineClass, req.Secret)
	if nil != err {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	client := apis.GetClientForTokenAndEndpoint(string(req.Secret.Data["warrenToken"]), providerSpec.APIEndpointURL)
	zone := client.LocationSlug

	if zone != "" && zone != providerSpec.Zone {
		return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Zone requested does not match expected client location: %q != %q", zone, providerSpec.Zone))
	}

	servers, err := client.VirtualMachine.ListVms()
	if nil != err {
		return nil, status.Error(codes.Unavailable, apis.GetServerErrorFromHttpCallError(err).Error())
	}

	listOfVMs := make(map[string]string)

	for _, server := range *servers {
		// @TODO: Replace name based filtering with labels if available
		if !strings.HasPrefix(server.Name, providerSpec.Cluster) {
			continue
		}

		listOfVMs[transcoder.EncodeProviderID(zone, server.Uuid)] = server.Name
	}

	return &driver.ListMachinesResponse{MachineList: listOfVMs}, nil
}

// GetVolumeIDs returns a list of Volume IDs for all PV Specs for whom an provider volume was found
//
// PARAMETERS
// ctx context.Context              Execution context
// req *driver.CreateMachineRequest The request object to get a list of VolumeIDs for a PVSpec
func (p *MachineProvider) GetVolumeIDs(ctx context.Context, req *driver.GetVolumeIDsRequest) (*driver.GetVolumeIDsResponse, error) {
	// Log messages to track start and end of request
	klog.V(2).Infof("GetVolumeIDs request has been received for %q", req.PVSpecs)
	defer klog.V(2).Infof("GetVolumeIDs request has been processed successfully for %q", req.PVSpecs)

	return &driver.GetVolumeIDsResponse{}, status.Error(codes.Unimplemented, "")
}
