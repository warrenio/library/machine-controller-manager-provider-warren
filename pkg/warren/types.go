/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package warren contains the Warren Platform specific implementation to manage machines
package warren

type CreateMachineMethodData struct {
	ServerUUID   string
	FloatingIPID int
}

type CtxWrapDataKey string

const (
	warrenDefaultVMUsername = "root"
	// Constant warrenPasswordGeneratedLength is the length of the generated random password
	warrenPasswordGeneratedLength = 32
)
