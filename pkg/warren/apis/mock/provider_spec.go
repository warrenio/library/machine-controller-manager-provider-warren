/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package mock provides all methods required to simulate a Warren Platform environment
package mock

import (
	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren/apis"
)

const (
	TestImageName = "ubuntu-20.04"
	TestProviderSpec = "{\"zone\":\"cyc01\",\"imageName\":\"ubuntu-20.04\",\"cores\":1,\"memory\":1024,\"volumeSize\":1073741824,\"sshKey\":\"ssh-rsa invalid\"}"
	TestServerType = "cx11-ceph"
	TestZone = "cyc01"
	TestInvalidProviderSpec = "{\"test\":\"invalid\"}"
)

// ManipulateProviderSpec changes given provider specification.
//
// PARAMETERS
// providerSpec *apis.ProviderSpec      Provider specification
// data         map[string]interface{} Members to change
func ManipulateProviderSpec(providerSpec *apis.ProviderSpec, data map[string]interface{}) *apis.ProviderSpec {
	for key, value := range data {
		manipulateStruct(&providerSpec, key, value)
	}

	return providerSpec
}

// NewProviderSpec generates a new provider specification for testing purposes.
func NewProviderSpec() *apis.ProviderSpec {
	return &apis.ProviderSpec{
		Cluster: TestNamespace,
		Zone: TestZone,
		ImageName: TestImageName,
		Cores: 1,
		Memory: 1024,
		SSHKey: TestSSHKey,
		VolumeSize: 1073741824,
	}
}
