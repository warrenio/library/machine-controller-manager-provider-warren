/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package mock provides all methods required to simulate a Warren Platform environment
package mock

import (
	"fmt"
	"encoding/json"
	"net/http"
	"strings"

	"github.com/gardener/machine-controller-manager/pkg/apis/machine/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const (
	jsonImageData = `
{
	"os_name": "ubuntu",
	"display_name": "Ubuntu",
	"ui_position": 1,
	"is_default": true,
	"is_app_catalog": false,
	"icon": "...",
	"versions": [
		{
			"os_version": "21.04",
			"display_name": "21.04",
			"published": true
		},
		{
			"os_version": "20.04",
			"display_name": "20.04",
			"published": true
		}
	]
}
	`
	jsonServerDataTemplate = `
{
	"uuid": %q,
	"name": %q,
	"hostname": %q,
	"status": %q,
	"backup": false,
	"billing_account": 6,
	"created_at": "2018-02-22 14:24:30",
	"description": "Proudly copied from the Warren Platform Cloud API documentation",
	"id": 42,
	"mac": "52:54:00:59:44:d1",
	"memory": 2048,
	"os_name": "ubuntu",
	"os_version": "16.04",
	"private_ipv4": "10.1.14.251",
	"storage": [
		{
			"created_at": "2018-02-22 14:24:30.312877",
			"id": 42,
			"name": "sda",
			"pool": "default2",
			"primary": true,
			"replica": [],
			"shared": false,
			"size": 20,
			"type": "block",
			"updated_at": null,
			"user_id": 8,
			"uuid": "12345678-9abc-def0-1234-56789abcdef0"
		}
	],
	"tags": null,
	"updated_at": null,
	"user_id": 8,
	"username": "example",
	"vcpu": 2
}
	`
	TestServerNameTemplate = "machine-%s"
	TestServerUUID = "01234567-89ab-4def-0123-c56789abcdef"
)

// ManipulateMachine changes given machine data.
//
// PARAMETERS
// machine *v1alpha1.Machine      Machine data
// data    map[string]interface{} Members to change
func ManipulateMachine(machine *v1alpha1.Machine, data map[string]interface{}) *v1alpha1.Machine {
	for key, value := range data {
		if (strings.Index(key, "ObjectMeta") == 0) {
			manipulateStruct(&machine.ObjectMeta, key[11:], value)
		} else if (strings.Index(key, "Spec") == 0) {
			manipulateStruct(&machine.Spec, key[5:], value)
		} else if (strings.Index(key, "Status") == 0) {
			manipulateStruct(&machine.Status, key[7:], value)
		} else if (strings.Index(key, "TypeMeta") == 0) {
			manipulateStruct(&machine.TypeMeta, key[9:], value)
		} else {
			manipulateStruct(&machine, key, value)
		}
	}

	return machine
}

// NewMachine generates new v1alpha1 machine data for testing purposes.
//
// PARAMETERS
// serverUUID int Server ID to use for machine specification
func NewMachine(serverUUID string) *v1alpha1.Machine {
	machine := &v1alpha1.Machine{
		TypeMeta: metav1.TypeMeta{
			APIVersion: "machine.sapcloud.io",
			Kind:       "Machine",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf(TestServerNameTemplate, serverUUID),
			Namespace: TestNamespace,
		},
	}

	// Don't initialize providerID and node if serverUUID == ""
	if serverUUID != "" {
		machine.Spec = v1alpha1.MachineSpec{
			ProviderID: fmt.Sprintf("warren:///v1/%s/%s", TestZone, serverUUID),
		}
	}

	return machine
}

// NewMachineClass generates new v1alpha1 machine class data for testing purposes.
func NewMachineClass() *v1alpha1.MachineClass {
	return NewMachineClassWithProviderSpec([]byte(TestProviderSpec))
}

// NewMachineClassWithProviderSpec generates new v1alpha1 machine class data based on the given provider specification for testing purposes.
//
// PARAMETERS
// providerSpec []byte ProviderSpec to use
func NewMachineClassWithProviderSpec(providerSpec []byte) *v1alpha1.MachineClass {
	return &v1alpha1.MachineClass{
		ProviderSpec: runtime.RawExtension{
			Raw: providerSpec,
		},
	}
}

// newJsonServerData generates a JSON server data object for testing purposes.
//
// PARAMETERS
// serverUUID  string Server ID to use
// serverState string Server state to use
func newJsonServerData(serverUUID string, serverState string) string {
	testServerName := fmt.Sprintf(TestServerNameTemplate, serverUUID)
	return fmt.Sprintf(jsonServerDataTemplate, serverUUID, testServerName, testServerName, serverState)
}

// SetupVMEndpointOnMux configures a "/v1/user-resource/vm" endpoint on the mux given.
//
// PARAMETERS
// mux *http.ServeMux Mux to add handler to
func SetupVMEndpointOnMux(mux *http.ServeMux, emptyUntilCreated bool) {
	baseURL := "/v1/cyc01/user-resource/vm"
	isVMCreated := !emptyUntilCreated

	mux.HandleFunc(baseURL, func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Type", "application/json; charset=utf-8")

		if (strings.ToLower(req.Method) == "delete") {
			res.WriteHeader(http.StatusOK)
		} else if (strings.ToLower(req.Method) == "get") {
			queryParams := req.URL.Query()

			if (queryParams.Get("uuid") == TestServerUUID) {
				res.WriteHeader(http.StatusOK)
				res.Write([]byte(newJsonServerData(TestServerUUID, "started")))
			} else {
				res.WriteHeader(http.StatusNotFound)
				res.Write([]byte(`{ "errors": { "Error": "[404] Server not found" } }`))
			}
		} else if (strings.ToLower(req.Method) == "post") {
			jsonData := make([]byte, req.ContentLength)
			req.Body.Read(jsonData)

			var data map[string]interface{}

			jsonErr := json.Unmarshal(jsonData, &data)
			if jsonErr != nil {
				panic(jsonErr)
			}

			isVMCreated = true
			res.WriteHeader(http.StatusCreated)

			res.Write([]byte(newJsonServerData(TestServerUUID, "stopped")))
		} else {
			panic("Unsupported HTTP method call")
		}
	})

	mux.HandleFunc(fmt.Sprintf("%s/list", baseURL), func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Type", "application/json; charset=utf-8")

		if (strings.ToLower(req.Method) == "get") {
			res.WriteHeader(http.StatusOK)

			res.Write([]byte("["))

			if isVMCreated {
				res.Write([]byte(newJsonServerData(TestServerUUID, "running")))
			}

			res.Write([]byte("]"))
		} else {
			panic("Unsupported HTTP method call")
		}
	})

	mux.HandleFunc(fmt.Sprintf("%s/start", baseURL), func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Type", "application/json; charset=utf-8")

		if isVMCreated {
			res.WriteHeader(http.StatusOK)
			res.Write([]byte(newJsonServerData(TestServerUUID, "started")))
		} else {
			res.WriteHeader(http.StatusNotFound)
			res.Write([]byte(`{ "errors": { "Error": "[404] Server not found" } }`))
		}
	})
}

// SetupVMImagesEndpointOnMux configures a "/v1/config/vm_images" endpoint on the mux given.
//
// PARAMETERS
// mux *http.ServeMux Mux to add handler to
func SetupVMImagesEndpointOnMux(mux *http.ServeMux) {
	mux.HandleFunc("/v1/cyc01/config/vm_images", func(res http.ResponseWriter, req *http.Request) {
		res.Header().Add("Content-Type", "application/json; charset=utf-8")

		res.WriteHeader(http.StatusOK)

		res.Write([]byte(fmt.Sprintf("[ %s ]", jsonImageData)))
	})
}
