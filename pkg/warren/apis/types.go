/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis is the main package for Warren specific APIs
package apis

import (
	"errors"

	"gitlab.com/warrenio/library/go-client/warren"
)

type ServerImage struct {
	warren.BaseImage
	warren.BaseImageVersion
}

const warrenDefaultURL = "https://api.equinix.warren.io/v1"

var (
	ErrFloatingIPNotFound             = errors.New("Floating IP not found")
	ErrImageNotFound                  = errors.New("Image not found")
	ErrRateLimitExceeded              = errors.New("API rate limit exceeded error")
	ErrResourceTemporarilyUnavailable = errors.New("Resource temporarily unavailable")
	ErrServerIsLocked                 = errors.New("Server is locked")
	ErrServerNotFound                 = errors.New("Server not found")
	ErrUnknownInternal                = errors.New("Internal API error")
)
