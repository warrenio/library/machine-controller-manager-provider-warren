/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package transcoder is used for API related object transformations
package transcoder

import (
	"fmt"
	"net/url"
	"strings"
)

func DecodeServerDataFromProviderID(id string) (*ServerData, error) {
	if !strings.HasPrefix(id, "warren://") {
		return nil, fmt.Errorf("%w: Scheme check failed", ErrProviderIDInvalid)
	}

	idURL, err := url.Parse(id)
	if nil != err {
		return nil, fmt.Errorf("%w: %v", ErrProviderIDInvalid, err)
	}

	return DecodeServerDataFromURL(idURL)
}

// DecodeServerUUIDFromProviderID decodes the given ProviderID to extract the server ID.
//
// PARAMETERS
// providerID string Provider ID to parse
func DecodeServerUUIDFromProviderID(providerID string) (string, error) {
	serverData, err := DecodeServerDataFromProviderID(providerID)
	if err != nil {
		return "", err
	}

	return serverData.UUID, nil
}

// DecodeZoneFromProviderID decodes the given ProviderID to extract the datacenter zone.
//
// PARAMETERS
// providerID string Provider ID to parse
func DecodeZoneFromProviderID(providerID string) (string, error) {
	serverData, err := DecodeServerDataFromProviderID(providerID)
	if err != nil {
		return "", err
	}

	return serverData.Zone, nil
}

// EncodeProviderID encodes the ProviderID string based on the given zone and server ID.
//
// PARAMETERS
// zone     string Datacenter zone
// serverID string Server ID
func EncodeProviderID(zone string, serverUUID string) string {
	return fmt.Sprintf("warren:///v1/%s/%s", url.PathEscape(zone), url.PathEscape(serverUUID))
}
