/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package transcoder is used for API related object transformations
package transcoder

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

func DecodeServerDataFromPath(path string) (*ServerData, error) {
	data := strings.SplitN(path, "/", 4)

	if len(data) != 4 {
		return nil, fmt.Errorf("%w: %s", ErrProviderIDInvalid, path)
	}

	pathVersion, err := strconv.ParseUint(data[1][1:], 10, 8)
	if nil != err {
		return nil, fmt.Errorf("%w: %s", ErrProviderIDInvalid, err.Error())
	}

	switch pathVersion {
		case 1:
			return &ServerData{ UUID: data[3], Zone: data[2] }, nil
		default:
			return nil, fmt.Errorf("%w: %s", ErrProviderIDInvalid, path)
	}
}

func DecodeServerDataFromURL(url *url.URL) (*ServerData, error) {
	if url.Scheme != "warren" {
		return nil, fmt.Errorf("%w: Scheme check failed", ErrProviderIDInvalid)
	}

	return DecodeServerDataFromPath(url.Path)
}
