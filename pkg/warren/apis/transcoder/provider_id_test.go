/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package transcoder is used for API related object transformations
package transcoder

import (
	"fmt"

	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren/apis/mock"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("ProviderID", func() {
	Describe("#DecodeServerDataFromProviderID", func() {
		It("should correctly parse and return decoded server information", func() {
			serverData, err := DecodeServerDataFromProviderID(EncodeProviderID(mock.TestZone, mock.TestServerUUID))

			Expect(err).NotTo(HaveOccurred())
			Expect(serverData.Zone).To(Equal(mock.TestZone))
			Expect(serverData.UUID).To(Equal(mock.TestServerUUID))
		})

		It("should fail if an unsupported provider ID scheme is provided", func() {
			_, err := DecodeServerDataFromProviderID("invalid:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no server ID", func() {
			_, err := DecodeServerDataFromProviderID("warren:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no zone", func() {
			_, err := DecodeServerDataFromProviderID("warren:///v1")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains an invalid server ID", func() {
			_, err := DecodeServerDataFromProviderID("warren:///test/nan")

			Expect(err).To(HaveOccurred())
		})
	})

	Describe("#DecodeZoneFromProviderID", func() {
		It("should correctly parse and return a zone", func() {
			zone, err := DecodeZoneFromProviderID(EncodeProviderID(mock.TestZone, mock.TestServerUUID))

			Expect(err).NotTo(HaveOccurred())
			Expect(zone).To(Equal(mock.TestZone))
		})

		It("should fail if an unsupported provider ID scheme is provided", func() {
			_, err := DecodeZoneFromProviderID("invalid:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no server ID", func() {
			_, err := DecodeZoneFromProviderID("warren:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no zone", func() {
			_, err := DecodeZoneFromProviderID("warren:///v1")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains an invalid server ID", func() {
			_, err := DecodeZoneFromProviderID("warren:///test/nan")

			Expect(err).To(HaveOccurred())
		})
	})

	Describe("#DecodeServerUUIDFromProviderID", func() {
		It("should correctly parse and return a server ID", func() {
			serverID, err := DecodeServerUUIDFromProviderID(EncodeProviderID(mock.TestZone, mock.TestServerUUID))

			Expect(err).NotTo(HaveOccurred())
			Expect(serverID).To(Equal(mock.TestServerUUID))
		})

		It("should fail if an unsupported provider ID scheme is provided", func() {
			_, err := DecodeServerUUIDFromProviderID("invalid:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no server ID", func() {
			_, err := DecodeServerUUIDFromProviderID("warren:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no zone", func() {
			_, err := DecodeServerUUIDFromProviderID("warren:///v1")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains an invalid server ID", func() {
			_, err := DecodeServerUUIDFromProviderID("warren:///test/nan")

			Expect(err).To(HaveOccurred())
		})
	})

	Describe("#DecodeServerUUIDFromProviderID", func() {
		It("should correctly parse and return a server ID", func() {
			serverID, err := DecodeServerUUIDFromProviderID(EncodeProviderID(mock.TestZone, mock.TestServerUUID))

			Expect(err).NotTo(HaveOccurred())
			Expect(serverID).To(Equal(mock.TestServerUUID))
		})

		It("should fail if an unsupported provider ID scheme is provided", func() {
			_, err := DecodeServerUUIDFromProviderID("invalid:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no server ID", func() {
			_, err := DecodeServerUUIDFromProviderID("warren:///test")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains no zone", func() {
			_, err := DecodeServerUUIDFromProviderID("warren:///v1")

			Expect(err).To(HaveOccurred())
		})
		It("should fail if a provider ID definition contains an invalid server ID", func() {
			_, err := DecodeServerUUIDFromProviderID("warren:///test/nan")

			Expect(err).To(HaveOccurred())
		})
	})

	Describe("#EncodeProviderID", func() {
		It("should correctly encode a provider ID", func() {
			providerID := EncodeProviderID(mock.TestZone, mock.TestServerUUID)
			Expect(providerID).To(Equal(fmt.Sprintf("warren:///v1/%s/%s", mock.TestZone, mock.TestServerUUID)))
		})
	})
})
