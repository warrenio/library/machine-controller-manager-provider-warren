/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis is the main package for Warren specific APIs
package apis

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/warrenio/library/go-client/warren"
	"k8s.io/klog/v2"
)

var singletons = make(map[string]*warren.Client)

// GetClientForToken returns an underlying Warren client for the given token.
//
// PARAMETERS
// token string Token to look up client instance for
func GetClientForToken(token string) *warren.Client {
	return GetClientForTokenAndEndpoint(token, "")
}

// GetClientForTokenAndEndpoint returns an underlying Warren client for the given token and API endpoint URL.
//
// PARAMETERS
// token string Token to look up client instance for
// url   string API endpoint URL
func GetClientForTokenAndEndpoint(token, url string) *warren.Client {
	var client *warren.Client

	if url == "" {
		client, _ = singletons[token]

		if nil == client {
			url = os.Getenv("WARREN_API_URL")

			if "" == url {
				url = warrenDefaultURL
			}
		}
	}

	if nil == client {
		if url[len(url)-1:] == "/" {
			url = url[:len(url)-1]
		}

		var location string

		if strings.Count(url, "/") == 3 {
			location = os.Getenv("WARREN_API_LOCATION")
		} else {
			urlData := strings.Split(url, "/")

			location = urlData[len(urlData)-1]
			url = strings.Join(urlData[0:len(urlData)-1], "/")
		}

		clientBuilder := (&warren.ClientBuilder{}).ApiUrl(url).ApiToken(token)
		if location != "" {
			clientBuilder = clientBuilder.LocationSlug(location)
		}

		var err error

		client, err = clientBuilder.Build()
		if nil != err {
			klog.Errorf("Warren platform client initialization failed: %w", err)
		}
	}

	return client
}

func GetErrorFromHttpCallError(err error) error {
	if nil == err {
		return nil
	}

	errString := err.Error()

	if strings.HasPrefix(errString, "[") && strings.Index(errString, "]") == 4 {
		switch errString[1:4] {
		case "400":
		case "500":
			if strings.Index(errString, "temporarily unavailable due to high demand") > -1 {
				return ErrResourceTemporarilyUnavailable
			}

			return fmt.Errorf("%w: %w", ErrUnknownInternal, err)
		case "429":
			return ErrRateLimitExceeded
		}
	}

	return err
}

// SetClientForToken sets a preconfigured Warren client for the given token.
//
// PARAMETERS
// token  string         Token to look up client instance for
// client *warren.Client Preconfigured Warren client
func SetClientForToken(token string, client *warren.Client) {
	if client == nil {
		delete(singletons, token)
	} else {
		singletons[token] = client
	}
}
