/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package validation - validation is used to validate cloud specific ProviderSpec
package validation

import (
	"fmt"

	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren/apis"
	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren/apis/mock"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
)

var _ = Describe("Validation", func() {
	providerSecret := &corev1.Secret{
		Data: map[string][]byte{
			"userData": []byte("dummy-user-data"),
		},
	}

	Describe("#ValidateWarrenProviderSpec", func() {
		type setup struct {
		}

		type action struct {
			spec   *apis.ProviderSpec
			secret *corev1.Secret
		}

		type expect struct {
			errToHaveOccurred bool
			errList           []error
		}

		type data struct {
			setup  setup
			action action
			expect expect
		}

		DescribeTable("##table",
			func(data *data) {
				errList := ValidateWarrenProviderSpec(data.action.spec, data.action.secret)

				if data.expect.errToHaveOccurred {
					Expect(errList).NotTo(BeNil())
					Expect(errList).To(Equal(data.expect.errList))
				} else {
					Expect(errList).To(BeEmpty())
				}
			},

			Entry("Simple validation of warren.io machine class", &data{
				setup: setup{},
				action: action{
					spec: mock.NewProviderSpec(),
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: false,
				},
			}),
			Entry("zone field missing", &data{
				setup: setup{},
				action: action{
					spec: &apis.ProviderSpec{
						ImageName: mock.TestImageName,
						Cores: 1,
						Memory: 1024,
						VolumeSize: 1073741824,
						SSHKey: mock.TestSSHKey,
					},
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: true,
					errList: []error{
						fmt.Errorf("zone is a required field"),
					},
				},
			}),
			Entry("imageName field missing", &data{
				setup: setup{},
				action: action{
					spec: &apis.ProviderSpec{
						Zone: mock.TestZone,
						Cores: 1,
						Memory: 1024,
						VolumeSize: 1073741824,
						SSHKey: mock.TestSSHKey,
					},
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: true,
					errList: []error{
						fmt.Errorf("imageName is a required field"),
					},
				},
			}),
			Entry("cores field missing", &data{
				setup: setup{},
				action: action{
					spec: &apis.ProviderSpec{
						Zone: mock.TestZone,
						ImageName: mock.TestImageName,
						Memory: 1024,
						VolumeSize: 1073741824,
						SSHKey: mock.TestSSHKey,
					},
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: true,
					errList: []error{
						fmt.Errorf("cores is a required field"),
					},
				},
			}),
			Entry("memory field missing", &data{
				setup: setup{},
				action: action{
					spec: &apis.ProviderSpec{
						Zone: mock.TestZone,
						ImageName: mock.TestImageName,
						Cores: 1,
						VolumeSize: 1073741824,
						SSHKey: mock.TestSSHKey,
					},
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: true,
					errList: []error{
						fmt.Errorf("memory is a required field"),
					},
				},
			}),
			Entry("volumeSize field missing", &data{
				setup: setup{},
				action: action{
					spec: &apis.ProviderSpec{
						Zone: mock.TestZone,
						ImageName: mock.TestImageName,
						Cores: 1,
						Memory: 1024,
						SSHKey: mock.TestSSHKey,
					},
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: true,
					errList: []error{
						fmt.Errorf("volumeSize is a required field"),
					},
				},
			}),
			Entry("sshKey field missing", &data{
				setup: setup{},
				action: action{
					spec: &apis.ProviderSpec{
						Zone: mock.TestZone,
						ImageName: mock.TestImageName,
						Cores: 1,
						Memory: 1024,
						VolumeSize: 1073741824,
					},
					secret: providerSecret,
				},
				expect: expect{
					errToHaveOccurred: true,
					errList: []error{
						fmt.Errorf("sshKey is a required field"),
					},
				},
			}),
		)
	})
})
