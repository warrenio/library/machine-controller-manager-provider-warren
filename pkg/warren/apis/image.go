/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis is the main package for Warren specific APIs
package apis

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/warrenio/library/go-client/warren"
)

func GetImageErrorFromHttpCallError(err error) error {
	if nil == err {
		return nil
	}

	errString := err.Error()

	if strings.HasPrefix(errString, "[") && strings.Index(errString, "]") == 4 {
		switch errString[1:4] {
		case "404":
			return ErrImageNotFound
		default:
			return GetErrorFromHttpCallError(err)
		}
	}

	return err
}

func GetImageWithName(ctx context.Context, client *warren.Client, name string) (*ServerImage, error) {
	images, err := client.VirtualMachine.ListBaseImages()
	if nil != err {
		return nil, GetImageErrorFromHttpCallError(err)
	}

	for _, image := range *images {
		for _, version := range image.Versions {
			osVersionSuffix := fmt.Sprintf("-%s", version.OsVersion)

			if strings.HasSuffix(name, osVersionSuffix) {
				if image.OsName == strings.Replace(name, osVersionSuffix, "", 1) {
					return &ServerImage{ image, version }, nil
				}
			}
		}
	}

	return nil, fmt.Errorf("%w: No match found for name %s", ErrImageNotFound, name)
}
