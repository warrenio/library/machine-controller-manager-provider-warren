/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis is the main package for Warren specific APIs
package apis

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/warrenio/library/go-client/warren"
)

func GetServerErrorFromHttpCallError(err error) error {
	if nil == err {
		return nil
	}

	errString := err.Error()

	if strings.HasPrefix(errString, "[") && strings.Index(errString, "]") == 4 {
		switch errString[1:4] {
		case "400":
			if strings.Index(errString, "No such virtual machine exists") > -1 {
				return ErrServerNotFound
			}

			return GetErrorFromHttpCallError(err)
		case "404":
			return ErrServerNotFound
		case "409":
			return ErrServerIsLocked
		default:
			return GetErrorFromHttpCallError(err)
		}
	}

	return err
}

func GetServerWithName(ctx context.Context, client *warren.Client, name string) (*warren.VirtualMachine, error) {
	servers, err := client.VirtualMachine.ListVms()
	if nil != err {
		return nil, GetServerErrorFromHttpCallError(err)
	}

	for _, server := range *servers {
		if server.Name == name {
			return &server, nil
		}
	}

	return nil, fmt.Errorf("%w: No match found for name %s", ErrServerNotFound, name)
}
