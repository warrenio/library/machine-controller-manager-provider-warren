/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package apis is the main package for Warren specific APIs
package apis

// ProviderSpec is the spec to be used while parsing the calls.
type ProviderSpec struct {
	Cluster    string `json:"cluster"`
	Zone       string `json:"zone"`
	Cores      uint   `json:"cores"`
	Memory     uint   `json:"memory"`
	VolumeSize uint   `json:"volumeSize"`
	ImageName  string `json:"imageName"`
	SSHKey     string `json:"sshKey"`

	Username         string `json:"username,omitempty"`
	Password         string `json:"password,omitempty"`
	APIEndpointURL   string `json:"apiEndpointURL,omitempty"`
	FloatingPoolName string `json:"floatingPoolName,omitempty"`
	NetworkUUID      string `json:"NetworkUUID,omitempty"`

	ExtraConfig map[string]string `json:"extraConfig,omitempty"`
}
