/*
Copyright 2022 OYE Network OÜ. All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Package main provides the application's entry point
package main

import (
	"fmt"
	"os"

	_ "github.com/gardener/machine-controller-manager/pkg/util/client/metrics/prometheus" // for client metric registration
	"github.com/gardener/machine-controller-manager/pkg/util/provider/app"
	"github.com/gardener/machine-controller-manager/pkg/util/provider/app/options"
	"github.com/spf13/pflag"
	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/spi"
	"gitlab.com/warrenio/library/machine-controller-manager-provider-warren/pkg/warren"
	"k8s.io/component-base/cli/flag"
	"k8s.io/component-base/logs"
	"k8s.io/component-base/version/verflag"
)

// main is the executable entry point.
func main() {
	s := options.NewMCServer()

	s.AddFlags(pflag.CommandLine)
	flag.InitFlags()

	verflag.PrintAndExitIfRequested()

	logs.InitLogs()
	defer logs.FlushLogs()

	err := app.Run(s, warren.NewWarrenProvider(&spi.PluginSPIImpl{}))
	if nil != err {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
