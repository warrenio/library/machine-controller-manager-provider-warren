# Machine Controller Manager Provider Warren

DISCLAIMER: Manual deployment of the manager to any Warren supporting platform is currently not officially supported.

Project Gardener implements the automated management and operation of [Kubernetes](https://kubernetes.io) clusters as a service. Its main principle is to leverage Kubernetes concepts for all of its tasks.

The Machine Controller Manager Provider Warren is an out of tree [implementation](https://github.com/gardener/machine-controller-manager) for the Warren platform.

## Driver Deployment

Please see `example` to how to deploy the Machine Controller Manager Provider Warren.
