# builder
FROM golang:alpine AS builder

ENV BINARY_PATH=/go/bin
WORKDIR /go/src/gitlab.com/warrenio/library/machine-controller-manager-provider-warren

RUN apk add bash git make

COPY . .
RUN make build

# base
# @TODO: Replace distroless/static-debian12 with alpine as long as we need to symlink to /machine-controller
FROM alpine:latest as base

WORKDIR /

# machine-controller-manager-provider-warren
FROM base AS machine-controller-manager-provider-warren
LABEL org.opencontainers.image.source="https://gitlab.com/warrenio/library/machine-controller-manager-provider-warren"

COPY --from=builder /go/bin/machine-controller-manager-provider-warren /bin/machine-controller-manager-provider-warren
RUN ln -s /bin/machine-controller-manager-provider-warren /machine-controller
ENTRYPOINT ["/bin/machine-controller-manager-provider-warren"]
