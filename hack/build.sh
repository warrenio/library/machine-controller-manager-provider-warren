#!/usr/bin/env bash

# Copyright (c) 2021 SAP SE or an SAP affiliate company. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

. $(dirname "$0")/prepare-source-path.sh

# If no LOCAL_BUILD environment variable is set, we configure the `go build` command
# to build for linux OS, amd64 architectures and without CGO enablement.
if [[ -z "$LOCAL_BUILD" ]]; then
  CGO_ENABLED=0 GOOS=$(go env GOOS) GOARCH=$(go env GOARCH) go build \
    -a \
    -v \
    -o ${BINARY_PATH}/machine-controller-manager-provider-warren \
    cmd/machine-controller-manager-provider-warren/main.go

# If the LOCAL_BUILD environment variable is set, we simply run `go build`.
else
  GOOS=$(go env GOOS) GOARCH=$(go env GOARCH) go build \
    -v \
    -o ${BINARY_PATH}/machine-controller-manager-provider-warren \
    cmd/machine-controller-manager-provider-warren/main.go
fi

echo "Build script finished"
